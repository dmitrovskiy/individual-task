<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>

<div class="container">
    <div class="row primary-content">
    <?php
        require_once 'model.php';

        $updateId = $_GET['id'];

        if(empty($updateId)) {
            die('error: id isn\'t set');
        }

        $fullname = $_POST['fullname'];
        $spec = $_POST['specialization'];
        $exp = $_POST['experience'];
        $model->update($updateId, $fullname, $spec, $exp);
        echo 'data has been updated';

    ?>

    <div><a href="index.php">Go home</a></div>

    </div>
</div>

</body>
</html>
