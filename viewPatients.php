<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<?php
    require_once 'model.php';

    if(isset($_GET['id'])):

    $data = $model->readByDoctorId($_GET['id']);


?>

<div class="container">
    <div class="row primary-content">
        <div class="col-md-8">
        <table class="table table-bordered">
            <caption>
                <h3>Patients</h3>
            </caption>
            <thead>
            <tr>
                <th>id</th>
                <th>doctor_id</th>
                <th>full name</th>
                <th>birth date</th>
                <th>address</th>
                <th>update</th>
                <th>delete</th>
            </tr>
            </thead>
            <tbody></tbody>
            <?php foreach($data as $row): ?>
                <tr>
                    <?php foreach($row as $rowItem): ?>
                        <td>
                            <?php echo $rowItem; ?>
                        </td>
                    <?php endforeach ?>
                    <td>
                        <a href="updatePatient.php?id=<?php echo $row['id']; ?>">update</a>
                    </td>
                    <td>
                        <form action="deletePatient.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>"/>
                            <input class="btn btn-danger btn-sm" type="submit" value="X"/>
                        </form>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
    <div class="col-md-4">
        <header class="row">
            <div class="col-md-8 col-md-offset-4">
                <h4>Create</h4>
            </div>
        </header>

        <form action="createPatient.php" method="post" class="form-horizontal">
            <input type="hidden" name="id_doctor" value="<?php echo $_GET['id']; ?>"/>

            <div class="form-group">
                <label class="col-md-4 control-label" for="fullname">Full name</label>
                <div class="col-md-8">
                    <input id="fullname" class="form-control" type="text" name="fullname"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="birth">Birth</label>
                <div class="col-md-8">
                    <input id="birth" class="form-control" type="text" name="birth"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="address">Address</label>
                <div class="col-md-8">
                    <input id="address" class="form-control" type="text" name="address"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <input class="btn btn-success" type="submit" value="create"/>
                </div>
            </div>
        </form>
        <?php else: ?>

            <p class="text-warning">
                <?php die('id of patient does not set'); ?>
            </p>

        <?php endif; ?>

</body>
</html>