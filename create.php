<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>

<div class="container">
    <div class="row primary-content">
    <?php
        require_once 'model.php';

        if(empty($_POST)) {
            die('data is not set');
        }

        $fullname = $_POST['fullname'];
        $spec = $_POST['specialization'];
        $exp = $_POST['experience'];

        $model->create($fullname, $spec, $exp);

        echo 'data has been created';
    ?>
    <div><a href="index.php"> Go home </a></div>
    </div>
</div>

</body>
</html>