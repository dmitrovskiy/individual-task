<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<?php
require_once 'model.php';

$updateId = $_GET['id'];

if(empty($updateId)) {
    die('error: id isn\'t set');
}

$updateData = $model->readOnePatient($updateId);
?>
<div class="container">
    <div class="row primary-content">
        <div class="col-md-4">
            <form class="form-horizontal" action="updatePatientAction.php?id=<?php echo $updateId; ?>" method="post">
                <?php foreach($updateData as $key => $value): ?>
                    <?php if($key !== 'id' && $key !== 'id_doctor'): ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="<?php echo $key; ?>">
                                <?php echo $key; ?>
                            </label>
                            <div class="col-md-8">
                                <input class="form-control" id="<?php echo $key; ?>" type="text" value="<?php echo $value; ?>" name="<?php echo $key; ?>"/>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <input class="btn btn-success" type="submit" value="update"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>