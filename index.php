<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Doctors</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <?php
        require_once 'model.php';
        $data = $model->read();
    ?>

    <div class="container">
        <div class="row primary-content">
            <div class="col-md-8">
                <table class="table table-bordered">
                    <caption><h3>Doctors</h3></caption>
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>full name</th>
                            <th>specialization</th>
                            <th>experience</th>
                            <th>update</th>
                            <th>patients</th>
                            <th>delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data as $row): ?>
                        <tr class="<?php echo ($row['experience'] < 5? '' : ($row['experience'] < 10?'info' : ($row['experience'] < 15? 'danger': 'danger'))); ?>">
                            <?php foreach($row as $rowItem): ?>
                                <td>
                                    <?php echo $rowItem; ?>
                                </td>
                            <?php endforeach ?>
                            <td>
                                <a href="update.php?id=<?php echo $row['id']; ?>">update</a>
                            </td>
                            <td>
                                <a href="viewPatients.php?id=<?php echo $row['id']; ?>">show</a>
                            </td>
                            <td>
                                <form action="delete.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>"/>
                                    <input type="submit" value="X" class="btn btn-danger btn-sm"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>

            <div class="col-md-4">
                <header class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <h4>Create</h4>
                    </div>
                </header>

                <form action="create.php" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="fullname">Full name:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="fullname"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="specialization">Specialization:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="specialization"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="experience">Experience:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="experience"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <input class="btn btn-success" type="submit" value="create"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>