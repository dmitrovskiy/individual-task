<?php

class Model
{
    private $mysql;

    public function __construct($user, $password, $host, $database)
    {
        $this->mysql = mysqli_connect($host, $user, $password, $database);
    }

    public function read()
    {
        $query = "SELECT * FROM doctors";
        return $this->executeQuery($query);
    }

    public function readOne($id) {
        $query = "SELECT * FROM doctors WHERE id=$id";
        return $this->executeQuery($query, true);
    }

    public function readByDoctorId($id)
    {
        $query = "SELECT * FROM patients WHERE id_doctor=$id";
        return $this->executeQuery($query);
    }

    public function readOnePatient($id)
    {
        $query = "SELECT * FROM patients WHERE id=$id";
        return $this->executeQuery($query, true);
    }

    public function updatePatient($id, $fullname, $birth, $address)
    {
        $query =
            "UPDATE patients SET fullname = '$fullname', birth = '$birth', address = '$address' WHERE id = $id";
        return $this->executeQuery($query, true);
    }

    public function delete($id)
    {
        $query = "DELETE FROM doctors WHERE id = $id";
        return $this->executeQuery($query, true);
    }

    public function deletePatient($id)
    {
        $query = "DELETE FROM patients WHERE id = $id";
        return $this->executeQuery($query, true);
    }

    public function update($id, $fullname, $spec, $exp)
    {
        $query
            = "UPDATE doctors SET fullname = '$fullname', specialization = '$spec', experience=$exp WHERE id = $id";
        return $this->executeQuery($query, true);
    }

    public function create($fullname, $spec, $exp)
    {
        $query = "INSERT INTO doctors (fullname, specialization, experience) VALUES('$fullname', '$spec', $exp)";
        return $this->executeQuery($query);
    }

    public function createPatient($doctorId, $fullname, $birth, $address)
    {
        $query = "INSERT INTO patients (id_doctor, fullname, birth, address) VALUES($doctorId, '$fullname', '$birth', '$address')";
        return $this->executeQuery($query, true);
    }

    /**
     * @param $query
     *
     * @return array
     */
    protected function executeQuery($query, $single = false)
    {
        $result = mysqli_query($this->mysql, $query);
        if(!$result instanceof mysqli_result)
            return $result;

        $resultData = array();
        while($row = mysqli_fetch_assoc($result))
        {
            array_push($resultData, $row);
        }

        return $single && isset($resultData['0']) ? $resultData[0] : $resultData;
    }
}

$model = new Model('root', '123', 'localhost', 'doctors');