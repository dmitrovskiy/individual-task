<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>

<div class="container">
    <div class="row primary-content">
    <?php

        $deleteId = $_POST['id'];
        if(empty($deleteId)) {
            echo "error occurred";
        }

        require_once 'model.php';

        $model->delete($deleteId);
        echo "Data has been deleted successfully";

    ?>

    <div>
        <a href="index.php">Go home</a>
    </div>

    </div>
</div>

</body>
</html>