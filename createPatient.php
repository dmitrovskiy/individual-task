<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>

<div class="container">
    <div class="row primary-content">
    <?php
        require_once 'model.php';

        if(empty($_POST)) {
            die('data is not set');
        }

        $id_doctor = $_POST['id_doctor'];
        $fullname = $_POST['fullname'];
        $birth = $_POST['birth'];
        $address = $_POST['address'];

        $model->createPatient($id_doctor, $fullname, $birth, $address);

        echo 'data has been created';
    ?>

    <div>
        <a href="index.php"> Go home </a>
    </div>
    </div>
</div>

</body>
</html>